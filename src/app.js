import React from 'react';
import './css';
import { Header, Elements, Showcases } from './components';

const App = () => { 
  return (
    <div className="main-container">
      <Header />
      <Elements />
      <Showcases />
    </div>
  );
}

export default App;