import React from 'react';

export default function Elements() {
  return (
    <main className="grid-container-4 mt-32">
      <div className="grid-item">
        <article>
          <h2>Frontend Development</h2>
          <i className="bi bi-globe2"></i>
          <p>
            More 2+ year in frontend development. Familiar with React.js, Angular.js, and pure HTML with CSS, JavaScript.
          </p>
          <p>
            I have joined two big scale project development, and two startup scale in frontend development mainly. 
          </p>
        </article>
      </div>
      <div className="grid-item">
        <article>
          <h2>Backend Development</h2>
          <i className="bi bi-columns"></i>
          <p>
            1+ year in backend development with using JAVA11/ JAVA6 for banking treasury system, and Node.js for food review entertainment platform and tutoring platform.
          </p>
          <p>Apart from the backend development, I also need to design the database structure with the method project.</p>
        </article>
      </div>
      <div className="grid-item">
        <article>
          <h2>Project Management</h2>
          <i className="bi bi-kanban"></i>
          <p>
            I am the delivery project manager in these two startups, I need to monitor the progress of the development work, and try need apply the "Agile" concept in the team.
          </p>
          <p>I certified the Professional Scrum Master I (PSM I) in 2021 June.</p>
        </article></div>   
      <div className="grid-item">
        <article>
          <h2>UI/ UX Design</h2>
          <i className="bi bi-front"></i>
          <p>
            I was the UI/ UX design lead in AfterSchool Edu Ltd in 2017 - 2018 to ensure the quality of the website's wireframes/ screens and mobile as well.
          </p>
          <p>
            I onganized the User acceptance testing (UAT) to our real-users to obtain the first-hand comments to enhance our solution.
          </p>
        </article>
      </div>
    </main>
  );
}
