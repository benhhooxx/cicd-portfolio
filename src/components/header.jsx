import React from "react";

export default function Header() {
	return (
		<header>
			<h1>
				Hello. I'm Ben Ho
				<span className="ml-8">
					<i className="bi bi-animation bi-heart-fill"></i>
				</span>
			</h1>
			<p className="quotation mb-32">
				"Welcome to my portfolio, there are my previous projects I worked
				below."
			</p>
			{/* <button className="ripple mb-16">Contact Me</button> */}
		</header>
	);
}
