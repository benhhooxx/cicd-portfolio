import React from 'react';

import { LazyImage } from '../helpers';

import Showcase1 from '../assets/showcase1.png';
import Showcase2 from '../assets/showcase2.png';
import Showcase3 from '../assets/showcase3.png';
import Showcase4 from '../assets/showcase4.png';

export default function Showcases() {

  return (
    <section className="mt-32">
      <h2>Showcases</h2>
      <div className="grid-container-2">
        <LazyImage key={1} src={Showcase1} alt={'showcase'} />
        <LazyImage key={2} src={Showcase2} alt={'showcase'} />
        <LazyImage key={3} src={Showcase3} alt={'showcase'} />
        <LazyImage key={4} src={Showcase4} alt={'showcase'} />
      </div>
    </section>
  );
}
